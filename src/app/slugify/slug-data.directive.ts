import { Directive, ElementRef } from '@angular/core';
import slugify from 'slugify';

@Directive({
    selector: '[slugData]', // eslint-disable-line @angular-eslint/directive-selector
    standalone: true
})
export class SlugDataDirective {

  constructor(private element: ElementRef<HTMLElement>) {
    const idAttr = element.nativeElement.getAttribute('id');
    if (idAttr) {
      element.nativeElement.setAttribute('data-slug', slugify(idAttr, { lower: true }));
    }
  }

}

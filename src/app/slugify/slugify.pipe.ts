import { Pipe, PipeTransform } from '@angular/core';
import slugify from 'slugify';

@Pipe({
    name: 'slugify',
    standalone: true
})
export class SlugifyPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    // Options : https://github.com/simov/slugify#options
    return slugify(value, {
      lower: true
    });
  }

}

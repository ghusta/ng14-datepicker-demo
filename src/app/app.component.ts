import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { NgbCollapse } from '@ng-bootstrap/ng-bootstrap';
import { ThemePickerComponent } from "./shared/theme-picker.component";
import * as bootstrap from 'bootstrap';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    imports: [NgbCollapse, RouterLink, RouterLinkActive, RouterOutlet, ThemePickerComponent]
})
export class AppComponent implements OnInit {
  title = 'angular-datepicker-demo';
  public isMenuCollapsed = true;

  constructor(@Inject(LOCALE_ID) public locale: string) {
  }

  ngOnInit(): void {
    this.initTooltips();
  }

  initTooltips() {
    // init BS ToolTips (https://getbootstrap.com/docs/5.3/components/tooltips/#enable-tooltips)
    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
    tooltipTriggerList.forEach(element => new bootstrap.Tooltip(element));
  }

}

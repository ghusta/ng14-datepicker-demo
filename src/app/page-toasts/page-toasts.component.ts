import { Component, OnDestroy } from '@angular/core';
import { ToastService } from '../toasts/toast.service';
import { ToastsContainerComponent } from '../toasts-container/toasts-container.component';

import { NgbToast, NgbTooltip } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-page-toasts',
    templateUrl: './page-toasts.component.html',
    styleUrls: ['./page-toasts.component.css'],
    imports: [NgbToast, NgbTooltip, ToastsContainerComponent]
})
export class PageToastsComponent implements OnDestroy {
  show = true;
  showWithButton = false;
  autohide = true;

  constructor(public toastService: ToastService) { }

  showStandard() {
    this.toastService.show('I am a standard toast');
  }

  showSuccess() {
    this.toastService.show('I am a success toast', { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(dangerTpl: any) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light', delay: 15000 });
  }

  ngOnDestroy(): void {
    this.toastService.clear();
  }

}

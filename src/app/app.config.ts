import '@angular/common/locales/global/fr';
import '@angular/common/locales/global/es';

import { importProvidersFrom, LOCALE_ID, ApplicationConfig } from '@angular/core';
import {provideRouter} from '@angular/router';

import {routes} from './app.routes';

export const appConfig: ApplicationConfig = {
  providers: [
    importProvidersFrom(),
    {provide: LOCALE_ID, useValue: 'fr-FR'},
    provideRouter(routes)
  ]
};

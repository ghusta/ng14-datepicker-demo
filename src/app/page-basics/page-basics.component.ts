import { FormatWidth, getLocaleDateFormat, DatePipe } from '@angular/common';
import { Component, Inject, LOCALE_ID } from '@angular/core';
import { SlugDataDirective } from '../slugify/slug-data.directive';

@Component({
    selector: 'app-page-basics',
    templateUrl: './page-basics.component.html',
    styleUrls: ['./page-basics.component.css'],
    imports: [SlugDataDirective, DatePipe]
})
export class PageBasicsComponent {

  shortDateFormat: string;
  shortDateFormatEnUs: string;
  shortDateFormatEs: string;
  mediumDateFormat: string;
  longDateFormat: string;

  constructor(@Inject(LOCALE_ID) public locale: string) {
    this.shortDateFormat = getLocaleDateFormat(locale, FormatWidth.Short);
    this.shortDateFormatEnUs = getLocaleDateFormat('en-US', FormatWidth.Short);
    this.shortDateFormatEs = getLocaleDateFormat('es', FormatWidth.Short);
    this.mediumDateFormat = getLocaleDateFormat(locale, FormatWidth.Medium);
    this.longDateFormat = getLocaleDateFormat(locale, FormatWidth.Long);
   }

}

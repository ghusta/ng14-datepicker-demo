import {Routes} from '@angular/router';

export const routes: Routes = [
  {path: '', loadComponent: () => import('./page-home/page-home.component').then(m => m.PageHomeComponent)},
  {path: 'basics', loadComponent: () => import('./page-basics/page-basics.component').then(m => m.PageBasicsComponent)},
  {path: 'datepickers', loadComponent: () => import('./page-datepickers/page-datepickers.component').then(m => m.PageDatepickersComponent)},
  {path: 'pagination', loadComponent: () => import('./page-pagination/page-pagination.component').then(m => m.PagePaginationComponent)},
  {path: 'toasts', loadComponent: () => import('./page-toasts/page-toasts.component').then(m => m.PageToastsComponent)},
  {path: 'table', loadComponent: () => import('./page-table/page-table.component').then(m => m.PageTableComponent)},
  {path: 'theme', loadComponent: () => import('./page-theme-colors/page-theme-colors.component').then(m => m.PageThemeColorsComponent)}
];

import { ChangeDetectionStrategy, Component, computed, effect, signal } from '@angular/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from "@angular/platform-browser";

/*
 * Inspired from https://github.com/ng-bootstrap/ng-bootstrap/blob/master/demo/src/app/shared/theme-picker.component.ts
 */

interface Theme {
  id: 'auto' | 'light' | 'dark';
  name: string;
  icon: string;
  iconSvg?: string;
}

const PREFERS_COLOR_SCHEME_DARK = window.matchMedia('(prefers-color-scheme: dark)');

const BI_SVG_SUN_FILL = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sun-fill" viewBox="0 0 16 16">
  <path d="M8 12a4 4 0 1 0 0-8 4 4 0 0 0 0 8M8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0m0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13m8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5M3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8m10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0m-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0m9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707M4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708"/>
</svg>`;

const BI_SVG_MOON_STARS_FILL = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-moon-stars-fill" viewBox="0 0 16 16">
  <path d="M6 .278a.77.77 0 0 1 .08.858 7.2 7.2 0 0 0-.878 3.46c0 4.021 3.278 7.277 7.318 7.277q.792-.001 1.533-.16a.79.79 0 0 1 .81.316.73.73 0 0 1-.031.893A8.35 8.35 0 0 1 8.344 16C3.734 16 0 12.286 0 7.71 0 4.266 2.114 1.312 5.124.06A.75.75 0 0 1 6 .278"/>
  <path d="M10.794 3.148a.217.217 0 0 1 .412 0l.387 1.162c.173.518.579.924 1.097 1.097l1.162.387a.217.217 0 0 1 0 .412l-1.162.387a1.73 1.73 0 0 0-1.097 1.097l-.387 1.162a.217.217 0 0 1-.412 0l-.387-1.162A1.73 1.73 0 0 0 9.31 6.593l-1.162-.387a.217.217 0 0 1 0-.412l1.162-.387a1.73 1.73 0 0 0 1.097-1.097zM13.863.099a.145.145 0 0 1 .274 0l.258.774c.115.346.386.617.732.732l.774.258a.145.145 0 0 1 0 .274l-.774.258a1.16 1.16 0 0 0-.732.732l-.258.774a.145.145 0 0 1-.274 0l-.258-.774a1.16 1.16 0 0 0-.732-.732l-.774-.258a.145.145 0 0 1 0-.274l.774-.258c.346-.115.617-.386.732-.732z"/>
</svg>`;

const BI_SVG_HALF_FILL = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-circle-half" viewBox="0 0 16 16">
  <path d="M8 15A7 7 0 1 0 8 1zm0 1A8 8 0 1 1 8 0a8 8 0 0 1 0 16"/>
</svg>`;

const THEMES: Theme[] = [
  {id: 'auto', name: 'Auto', icon: 'bi-circle-half', iconSvg: BI_SVG_HALF_FILL},
  {id: 'light', name: 'Light', icon: 'bi-sun-fill', iconSvg: BI_SVG_SUN_FILL},
  {id: 'dark', name: 'Dark', icon: 'bi-moon-stars-fill', iconSvg: BI_SVG_MOON_STARS_FILL},
];

@Component({
    selector: 'app-theme-picker',
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [NgbDropdownModule],
    template: `
		<div class="nav-item" ngbDropdown>
			<button class="nav-link" ngbDropdownToggle id="demo-site-theme" role="button" aria-label="Open theme picker">
				<span [innerHTML]="getSVGImage(current().iconSvg)"></span>
			</button>
			<div ngbDropdownMenu aria-labelledby="demo-site-theme" class="dropdown-menu dropdown-menu-end">
				@for (theme of themes(); track theme) {
					<button ngbDropdownItem [class.active]="theme.id === current().id" (click)="preferred.set(theme.id)">
            <span class="me-2" [innerHTML]="getSVGImage(theme.iconSvg)"></span>{{ theme.name }}
					</button>
				}
			</div>
		</div>
	`
})
export class ThemePickerComponent {

  themes = signal(THEMES).asReadonly();
  preferred = signal(localStorage.getItem('theme') || this.themes()[0].id);
  current = computed(() => this.themes().find((t) => t.id === this.preferred()) || this.themes()[0]);
  systemTheme = signal(PREFERS_COLOR_SCHEME_DARK.matches ? 'dark' : 'light');
  dataBsTheme = computed(() => (this.current().id === 'auto' ? this.systemTheme() : this.current().id));

  constructor(private _sanitizer: DomSanitizer) {
    // save preferred theme in local storage when it changes
    effect(() => localStorage.setItem('theme', this.preferred()));

    // log current theme in console
    effect(() => console.debug("Current theme is :", this.current().id));

    // updating current theme in DOM
    effect(() => document.documentElement.setAttribute('data-bs-theme', this.dataBsTheme()));

    // listening to system theme changes, could be done with RxJS, but this is simpler
    PREFERS_COLOR_SCHEME_DARK.addEventListener('change', (event) =>
      this.systemTheme.set(event.matches ? 'dark' : 'light'),
    );
  }

  getSVGImage(image?: string) {
    if (image) {
      return this._sanitizer.bypassSecurityTrustHtml(`${image}`);
    } else {
      return '';
    }
  }
}

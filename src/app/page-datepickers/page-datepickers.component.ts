import { Component, Inject, LOCALE_ID, OnInit, OnDestroy } from '@angular/core';
import { NgbDateStruct, NgbCalendar, NgbDate, NgbDatepicker, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { NgClass, JsonPipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'app-page-datepickers',
    templateUrl: './page-datepickers.component.html',
    styleUrls: ['./page-datepickers.component.css'],
    imports: [NgbDatepicker, FormsModule, NgbInputDatepicker, NgClass, JsonPipe]
})
export class PageDatepickersComponent implements OnInit, OnDestroy  {

  model: NgbDateStruct | undefined;
  model2: NgbDateStruct | undefined;

  constructor(@Inject(LOCALE_ID) public locale: string, private calendar: NgbCalendar) { }

  ngOnInit(): void {
    console.log('init PageDatepickersComponent');
  }

  ngOnDestroy(): void {
    console.log('destroy PageDatepickersComponent');
  }

  selectToday() {
    this.model = this.calendar.getToday();
  }

  onDateSelect($event: NgbDate) {
    console.log('Datepicker : selected : ', $event);
    // or JSON.stringify($event) ...
  }
}

import {Component} from '@angular/core';
import {NgbPagination} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-page-pagination',
    imports: [
        NgbPagination
    ],
    templateUrl: './page-pagination.component.html',
    styleUrl: './page-pagination.component.css'
})
export class PagePaginationComponent {
  pageBasic = 4;
  pageAdvanced = 6;

  isDisabled = true;

  toggleDisabled() {
    this.isDisabled = !this.isDisabled;
  }
}

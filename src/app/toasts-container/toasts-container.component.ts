import { Component, inject, TemplateRef } from '@angular/core';
import { ToastService } from '../toasts/toast.service';
import { NgbToast } from '@ng-bootstrap/ng-bootstrap';
import { NgTemplateOutlet } from '@angular/common';

@Component({
    selector: 'app-toasts-container',
    template: `
		@for (toast of toastService.toasts; track toast) {
		  <ngb-toast
		    [class]="toast.classname"
		    [autohide]="true"
		    [delay]="toast.delay || 5000"
		    (hidden)="toastService.remove(toast)"
		    >
		    @if (isTemplate(toast)) {
		      <ng-template [ngTemplateOutlet]="toast.textOrTpl"></ng-template>
		    } @else {
		      {{ toast.textOrTpl }}
		    }
		  </ngb-toast>
		}
		`,
    host: { class: 'toast-container position-fixed top-0 end-0 p-3', style: 'z-index: 1200' },
    imports: [NgbToast, NgTemplateOutlet]
})
export class ToastsContainerComponent {

	toastService = inject(ToastService);

  isTemplate(toast: any) {
    return toast.textOrTpl instanceof TemplateRef;
  }
}
